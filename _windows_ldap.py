import ldap


def ldap_auth(ip,mail,password):
    con = ldap.initialize('ldap://'+ip)
    con.protocol_version = 3
    con.set_option(ldap.OPT_REFERRALS, 0)
    try:
        result = con.simple_bind_s(mail,password)
        if result[0]:
            return True
    except ldap.INVALID_CREDENTIALS:
        #return "invalid credentials"
        return False
    except ldap.SERVER_DOWN:
        #return "Server down"
        return False
    except ldap.LDAPError, e:
        if type(e.message) == dict and e.message.has_key('desc'):
            #return "Other LDAP error: " + e.message['desc']
            return False
        else:
            #return "Other LDAP error: " + e
            return False
    finally:
        con.unbind_s()
    return False

    # ########## performing a simple ldap query ####################################
    # ldap_base = "dc=campinapolis,dc=net"
    # query = "(uid=carloseduardo)"
    # result = con.search_s(ldap_base, ldap.SCOPE_SUBTREE, query)
    # ########## adding (a user) ####################################################
    # # make sure all input strings are str and not unicode
    # # (it doesn't like unicode for some reason)
    # # each added attribute needs to be added as a list
    # dn = "uid=maarten,ou=people,dc=example,dc=com"
    # modlist = {
    #     "objectClass": ["inetOrgPerson", "posixAccount", "shadowAccount"],
    #     "uid": ["maarten"],
    #     "sn": ["De Paepe"],
    #     "givenName": ["Maarten"],
    #     "cn": ["Maarten De Paepe"],
    #     "displayName": ["Maarten De Paepe"],
    #     "uidNumber": ["5000"],
    #     "gidNumber": ["10000"],
    #     "loginShell": ["/bin/bash"],
    #     "homeDirectory": ["/home/maarten"]}
    # }
    # # addModList transforms your dictionary into a list that is conform to ldap input.
    # result = con.add_s(dn, ldap.modlist.addModlist(modlist))
    # ########## modifying (a user, or in this case, the user's password) ##########
    # # this works a bit strange.
    # # in a rel. database you just give the new value for the record you want to change
    # # here you need to give an old/new pair
    # dn = "uid=maarten,ou=people,dc=example,dc=com"
    # # you can expand this list with whatever amount of attributes you want to modify
    # old_value = {"userPassword": ["my_old_password"]}
    # new_value = {"userPassword": ["my_new_password"]}
    # modlist = ldap.modlist.modifyModlist(old_value, new_value)
    # con.modify_s(dn, modlist)
    # ########## deleting (a user) #################################################
    # dn = "uid=maarten,ou=people,dc=example,cd=com"
    # con.delete_s(dn)
    # return query


# def authenticate(address, username, password):
#     conn = ldap.initialize('ldap://' + address)
#     conn.protocol_version = 3
#     conn.set_option(ldap.OPT_REFERRALS, 0)
#     try:
#         result = conn.simple_bind_s(username, password)
#         base = "dc=campinapolis,dc=net"
#         scope = ldap.SCOPE_SUBTREE
#         filter = "(&(objectClass=user)(sAMAccountName=" + "carloseduardo" + "))"
#         attrs = ["*"]
#         result = conn.search(base, scope, filter, attrs)
#         type, user = conn.result(result, 60)
#         name, attrs = user[0]
#         print name
#     except ldap.INVALID_CREDENTIALS:
#         return "Invalid credentials"
#     except ldap.SERVER_DOWN:
#         return "Server down"
#     except ldap.LDAPError, e:
#         if type(e.message) == dict and e.message.has_key('desc'):
#             return "Other LDAP error: " + e.message['desc']
#         else:
#             return "Other LDAP error: " + e
#     finally:
#         conn.unbind_s()
#     return "Succesfully authenticated"

