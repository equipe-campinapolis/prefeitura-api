import bottle
import sqlite3
import jwt
import json
import base64
import warnings
import PIL
from PIL import Image
from json import dumps
from bottle import run, response, post, get, route, debug, auth_basic, request
from basicauth import decode
####################################################################
import _auth
import _variables


####################################################################
#####################################error codes####################
# id:0->
# id:1->
####################################################################


@post('/save_image', method=['OPTIONS', 'POST'])
@_auth.enable_cors
@_auth.autenticar
def save_image():
    print "SAVE_IMAGE"
    response.content_type = 'application/json'
    if _variables.loged_in:
        postdata = request.body.read()
        o = json.loads(postdata)
        _, b64data = o["base64Data"].split(',')
        f = open(o["path"]+"/"+str(o["imageFileName"])+".png", "wb")
        f.write(b64data.decode('base64'))
        f.close()
        try:
            image = Image.open(o["path"]+"/"+str(o["imageFileName"])+".png")
            new_image = image.resize((128, 128))
            new_image.save(o["path"]+"/"+str(o["imageFileName"])+"_128x128.png")
        except IOError:
            print "erro ao gerar imagem"
    else:
        _variables.loged_in = False
        return dumps([{"Mensagem": "acesso negado"}])
