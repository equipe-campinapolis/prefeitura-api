import bottle
import sqlite3
import jwt
import json
import base64 
import warnings
from json import dumps
from bottle import run,response,post, get,route,debug,auth_basic, request
from basicauth import decode
####################################################################
import _auth
import _variables
####################################################################

###########################
#allow all orgins ['*']
###########################

#####################################error codes####################
#id:2->acesso negado
#id:4->erro ao
####################################################################


# def enable_cors(fn):
# 	def _enable_cors(*args, **kwargs):
# 		domain = request.query.o
# 		response.headers['Access-Control-Allow-Origin'] = domain
# 		response.headers['Access-Control-Allow-Credentials'] = 'true'
# 		response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
# 		response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
# 		if bottle.request.method != 'OPTIONS':
# 			return fn(*args, **kwargs)
# 	return _enable_cors


def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d


# @_variables.app.route('/cors', method=['OPTIONS', 'GET'])
# @enable_cors
# def lvambience():
# 	response.headers['Content-type'] = 'application/json'
# 	response.headers['Access-Control-Allow-Credentials'] = 'true'
# 	return '[1]'


@get('/get_user', method=['OPTIONS', 'GET'])
@_auth.enable_cors
@_auth.autenticar
def get_user():
	print "GET_USER"
	response.content_type = 'application/json'
	if _variables.loged_in:
		#token = request.get_cookie('token')
		token = bottle.request.headers.get('Authorization', None)
		if type(token) is str:
			parts = token.split()
			token = parts[1]
			result = jwt.decode(token, _variables.secret)
			id = result["id"]
			conn = sqlite3.connect('usuarios.db')
			conn.row_factory = dict_factory
			c = conn.cursor()
			c.execute("SELECT * FROM usuarios WHERE id=?",(id,))
			result = c.fetchone()
			if result is not None:
				_variables.loged_in = False
				return dumps(result)
		else:
			_variables.loged_in = False
			return dumps([{"Mensagem":"acesso negado"}])
	else:
		_variables.loged_in = False
		return dumps([{"Mensagem": "acesso negado"}])


@get('/get_user_by_id', method=['OPTIONS', 'GET'])
@_auth.enable_cors
@_auth.autenticar
def get_user_by_id():
	response.content_type = 'application/json'
	print "GET_USER_BY_ID"
	if _variables.loged_in:
		id = request.query.id
		print id
		conn = sqlite3.connect('usuarios.db')
		conn.row_factory = dict_factory
		c = conn.cursor()
		c.execute("SELECT * FROM usuarios WHERE id=?",(id,))
		result = c.fetchone()
		if result is not None:
			_variables.loged_in = False
			return dumps(result)
		else:
			_variables.loged_in = False
			return dumps([{"Mensagem": "erro"}])
	else:
		_variables.loged_in = False
		return dumps([{"Mensagem": "acesso negado"}])


@get('/get_number_of_users', method=['OPTIONS', 'GET'])
@_auth.enable_cors
@_auth.autenticar
def get_number_of_users():
	response.content_type = 'application/json'
	if _variables.loged_in:
		_variables.loged_in = False
		conn = sqlite3.connect('usuarios.db')
		conn.row_factory = dict_factory
		cursor = conn.cursor()
		cursor.execute("select * from usuarios")
		results = cursor.fetchall()
		return {"quantidade":len(results)}
	else:
		_variables.loged_in = False
		return dumps([{"Mensagem": "acesso negado"}])


@get('/get_users', method=['OPTIONS', 'GET'])
@_auth.enable_cors
@_auth.autenticar
def get_users():
	response.content_type = 'application/json'
	if _variables.loged_in:
		_variables.loged_in = False
		conn = sqlite3.connect('usuarios.db')
		conn.row_factory = dict_factory
		cursor = conn.cursor()
		cursor.execute("select * from usuarios")
		results = cursor.fetchall()
		conn.close()
		return dumps(results)
	else:
		_variables.loged_in = False
		return dumps([{"id":2,"Mensagem": "acesso negado"}])


@post('/new_user', method=['OPTIONS','POST'])
@_auth.enable_cors
@_auth.autenticar
def new_user():
	response.content_type = 'application/json'
	try:
		info = request.json["info"]
	except Exception as e:
		print 'erro new_user '
	try:
		name = request.json["name"]
		mail = request.json["mail"]
		password = request.json["pass"]
	except Exception as e:
		return {"id": 0, "Mensagem": "erro"}
	if _variables.loged_in:
		conn = sqlite3.connect('usuarios.db')
		c = conn.cursor()
		try:
			if info != "":
				c.execute('''INSERT INTO usuarios (status,name,email,password,info) VALUES (?,?,?,?,?)''', ('1', name, mail, password,info,))
			else:
				c.execute('''INSERT INTO usuarios (status,name,email,password) VALUES (?,?,?,?)''',('1', name, mail, password,))
			conn.commit()
		except:
			conn.close()
			return dumps([{"id":4,"Mensagem":"Erro no banco de dados"}])
		conn.close()
		_variables.loged_in = False
		return dumps([{"id":1,"Mensagem": "criado com sucesso"}])
	else:
		_variables.loged_in = False
		return dumps([{"id":2,"Mensagem": "acesso negado"}])