import bottle
import sqlite3
import jwt
import json
import base64
import time
from bottle import run, get,route,debug,auth_basic,request,response
from basicauth import decode
from json import dumps
####################################################################
import _variables
import _windows_ldap
####################################################################


def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        #domain = request.query.o
        domain = request.headers["Origin"]
        response.headers['Access-Control-Allow-Origin'] = domain
        response.headers['Access-Control-Allow-Credentials'] = 'true'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        if bottle.request.method != 'OPTIONS':
            return fn(*args, **kwargs)
    return _enable_cors


def basic_auth(user, password):
    print "BASIC_AUTH"
    conn = sqlite3.connect('usuarios.db')
    c = conn.cursor()
    c.execute("SELECT id,username FROM usuarios WHERE username=? AND password=?",(user,password,))
    result = c.fetchone()
    if result is not None:
        _variables.base_loged_in = True
    return True


def domain_auth(func):
    def wrapper():
        user, password = decode(request.get_header('Authorization'))
        return func(_windows_ldap.ldap_auth(_variables.ldap_ip,user,password))
        # conn = sqlite3.connect('usuarios.db')
        # conn.row_factory = dict_factory
        # c = conn.cursor()
        # c.execute("SELECT id,username,email,password FROM usuarios WHERE username=? AND password=?",(user,password,))
        # result = c.fetchone()
        # if result is not None:
        #     return func(result)
        #     result['ts'] = ts
        #     result['status_id'] = 1
        #     token = jwt.encode(result, _variables.secret)
        #     return dumps([{"Mensagem":"token ok","token":token,"id":1}])
        # return dumps([{"Mensagem":"nao autenticado","id":0}])
    return wrapper


def autenticar(func):
    def wrapper():
        print "AUTENTICAR"
        auth = bottle.request.headers.get('Authorization', None)
        if not auth:
            _variables.loged_in = False
            return func()
        parts = auth.split()
        if parts[0].lower() != 'bearer':
            _variables.loged_in = False
            return func()
        elif len(parts) == 1:
            _variables.loged_in = False
            return func()
        elif len(parts) > 2:
            _variables.loged_in = False
            return func()
        token = parts[1]
        if type(token) is str:
            result = jwt.decode(token, _variables.secret)
            if result.has_key('status_id'):
                status = result['status_id']
                if status == 0:
                    #bottle.response.delete_cookie("token")
                    _variables.loged_in = False
                elif status == 1:
                    id = result["id"]
                    conn = sqlite3.connect('usuarios.db')
                    c = conn.cursor()
                    c.execute("SELECT id,username FROM usuarios WHERE id=?", (id,))
                    result = c.fetchone()
                    if result is not None:
                        _variables.loged_in = True
                    else:
                        _variables.loged_in = False
            else:
                _variables.loged_in = False
            return func()
        else:
            _variables.loged_in = False
    return wrapper


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


@_variables.app.route('/cors', method=['OPTIONS', 'GET'])
@enable_cors
def lvambience():
    response.headers['Content-type'] = 'application/json'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return '[1]'


@get('/entrar', method=['OPTIONS', 'GET'])
@enable_cors
@auth_basic(basic_auth)
def generate_jwt():
    response.content_type = 'application/json'
    print "GENERATE_JWT"
    if _variables.base_loged_in:
        user, password = decode(request.get_header('Authorization'))
        conn = sqlite3.connect('usuarios.db')
        conn.row_factory = dict_factory
        c = conn.cursor()
        c.execute("SELECT id,username FROM usuarios WHERE username=? AND password=?",(user,password,))
        result = c.fetchone()
        if result is not None:
            ts = time.time()
            result['ts'] = ts
            result['status_id'] = 1
            token = jwt.encode(result, _variables.secret)
            return dumps([{"Mensagem":"token ok","token":token,"id":1}])
        return dumps([{"Mensagem":"nao autenticado","id":0}])
    else:
        _variables.loged_in = False
        return dumps([{"Mensagem": "acesso negado","id":0}])


@get('/domain_entrar', method=['OPTIONS', 'GET'])
@enable_cors
@domain_auth
def domain_login(r):
    response.content_type = 'application/json'
    if r:
        user, password = decode(request.get_header('Authorization'))
        mail = user
        conn = sqlite3.connect('usuarios.db')
        conn.row_factory = dict_factory
        c = conn.cursor()
        c.execute("SELECT id,username,email,password FROM usuarios WHERE email=?",(mail,))
        result = c.fetchone()
        if result is not None:
            result['status_id'] = 1
            return dumps({"resp":result,"id":1})
        return dumps({"Mensagem":"nao autenticado","id":0})
    else:
        _variables.loged_in = False
        return dumps({"Mensagem": "acesso negado","id":0})


@get('/sair', method=['OPTIONS', 'GET'])
@enable_cors
def sair():
    response.content_type = 'application/json'
    if _variables.loged_in:
        token = request.get_cookie('token')
        if type(token) is str:
            result = jwt.decode(token, _variables.secret)
            result['status_id'] = 0
            token = jwt.encode(result, _variables.secret)
            #bottle.response.delete_cookie("token")
            bottle.response.set_cookie('token',token)
            _variables.loged_in = False
            return dumps([{"Mensagem":"ok saiu"}])
    else:
        _variables.loged_in = False
        return dumps([{"Mensagem": "ok saiu"}])
