import bottle
import sqlite3
import jwt
import json
import base64
import warnings
from json import dumps
from bottle import run, response, post, get, route, debug, auth_basic, request
from basicauth import decode
####################################################################
import _variables
import _auth
####################################################################
#####################################error codes####################
#id:0->"erro"
#id:1->"mensagem atualizada com sucesso"
####################################################################


# def enable_cors(fn):
#     def _enable_cors(*args, **kwargs):
#         domain = request.query.o
#         response.headers['Access-Control-Allow-Origin'] = domain
#         response.headers['Access-Control-Allow-Credentials'] = 'true'
#         response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
#         response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
#         if bottle.request.method != 'OPTIONS':
#             return fn(*args, **kwargs)
#     return _enable_cors


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


# def autenticar(func):
#     def wrapper():
#         token = request.get_cookie('token')
#         if type(token) is str:
#             result = jwt.decode(token, _variables.secret)
#             id = result["id"]
#             conn = sqlite3.connect('usuarios.db')
#             c = conn.cursor()
#             c.execute("SELECT id,username FROM usuarios WHERE id=?",(id,))
#             result = c.fetchone()
#             if result is not None:
#                 _variables.loged_in = True
#         else:
#             _variables.loged_in = False
#         return func()
#     return wrapper


@_variables.app.route('/cors', method=['OPTIONS', 'GET'])
@_auth.enable_cors
def lvambience():
    response.headers['Content-type'] = 'application/json'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return '[1]'


@post('/update_message_status', method=['OPTIONS', 'POST'])
@_auth.enable_cors
@_auth.autenticar
def update_message_status():
    response.content_type = 'application/json'
    try:
        message_id = request.json["message_id"]
        status = request.json["status"]
    except Exception as e:
        return {"id": 0, "Mensagem": "erro"}
    try:
        if _variables.loged_in:
            id = request.query.id
            conn = sqlite3.connect('mensagens.db')
            conn.row_factory = dict_factory
            c = conn.cursor()
            c.execute("UPDATE MESSAGES SET status=? WHERE id=?",(status,message_id,))
            conn.commit()
            conn.close()
            return dumps([{"id":1,"Mensagem": "atualizado com com sucesso"}])
    except Exception as e:
        _variables.loged_in = False
        return dumps([{"Mensagem": "erro"}])
    else:
        _variables.loged_in = False
        return dumps([{"Mensagem": "acesso negado"}])


@get('/get_message', method=['OPTIONS', 'GET'])
@_auth.enable_cors
@_auth.autenticar
def get_message():
    response.content_type = 'application/json'
    if _variables.loged_in:
        id = request.query.id
        conn = sqlite3.connect('mensagens.db')
        conn.row_factory = dict_factory
        c = conn.cursor()
        c.execute("SELECT * FROM messages WHERE id=?", (id,))
        result = c.fetchone()
        conn.close()
        if result is not None:
            _variables.loged_in = False
            return dumps(result)
        else:
            _variables.loged_in = False
            return dumps([{"Mensagem": "erro"}])
    else:
        _variables.loged_in = False
        return dumps([{"Mensagem": "acesso negado"}])


@get('/get_number_of_messages', method=['OPTIONS', 'GET'])
@_auth.enable_cors
def get_number_of_messages():
    response.content_type = 'application/json'
    conn = sqlite3.connect('mensagens.db')
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute("select * from mensagem")
    results = cursor.fetchall()
    conn.close()
    return {"quantidade": len(results)}


@post('/new_message', method=['OPTIONS','POST'])
@_auth.enable_cors
@_auth.autenticar
def new_message():
    response.content_type = 'application/json'
    try:
        site_id = request.json["site_id"]
        user_id = request.json["user_id"]
        type_id = request.json["type_id"]
        mensagem = request.json["mensagem"]
        source = request.json["source"]
        destiny = request.json["destiny"]
    except Exception as e:
        return {"id":0,"Mensagem": "erro"}
    if _variables.loged_in:
        conn = sqlite3.connect('mensagens.db')
        c = conn.cursor()
        try:
            c.execute('''INSERT INTO mensagem (site_id,user_id,type_id,mensagem,source,destiny) VALUES (?,?,?,?,?,?)''', (site_id,user_id,type_id,mensagem,source,destiny,))
            conn.commit()
            conn.close()
        except Exception as e:
            conn.close()
            _variables.loged_in = False
            return dumps([{"id":4,"Mensagem":"Erro no banco de dados","erro":str(e)}])
        return dumps([{"id":1,"Mensagem": "criado com sucesso"}])
    else:
        _variables.loged_in = False
        return dumps([{"id":2,"Mensagem": "acesso negado"}])


@post('/drop_message', method=['OPTIONS','POST'])
@_auth.enable_cors
@_auth.autenticar
def drop_message():
    response.content_type = 'application/json'
    try:
        message_id = request.json["message_id"]
    except Exception as e:
        return {"id":0,"Mensagem": "erro"}
    if _variables.loged_in:
        conn = sqlite3.connect('mensagens.db')
        c = conn.cursor()
        try:
            c.execute("DELETE FROM messages WHERE id=?", (message_id,))
            conn.commit()
            conn.close()
        except Exception as e:
            conn.close()
            _variables.loged_in = False
            return dumps([{"id":4,"Mensagem":"Erro no banco de dados","erro":str(e)}])
        return dumps([{"id":1,"Mensagem": "excluido com sucesso"}])
    else:
        conn.close()
        _variables.loged_in = False
        return dumps([{"id":2,"Mensagem": "acesso negado"}])