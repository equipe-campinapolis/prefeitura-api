import subprocess
import sys
import bottle
import sqlite3
import jwt
import json
import base64
import warnings
from json import dumps
from bottle import run,response,post, get,route,debug,auth_basic, request
from basicauth import decode
####################################################################
import _variables
####################################################################
#####################################error codes####################
#id:1->up
#id:2->down
#id:3->down
####################################################################


def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        domain = request.query.o
        response.headers['Access-Control-Allow-Origin'] = domain
        response.headers['Access-Control-Allow-Credentials'] = 'true'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Authorization, Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        if bottle.request.method != 'OPTIONS':
            return fn(*args, **kwargs)
    return _enable_cors


def autenticar(func):
    def wrapper():
        token = request.get_cookie('token')
        if type(token) is str:
            result = jwt.decode(token, _variables.secret)
            id = result["id"]
            conn = sqlite3.connect('usuarios.db')
            c = conn.cursor()
            c.execute("SELECT id,username FROM usuarios WHERE id=?",(id,))
            result = c.fetchone()
            if (result is not None):
                _variables.loged_in = True
        else:
            _variables.loged_in = False
        return func()
    return wrapper


@_variables.app.route('/cors', method=['OPTIONS', 'GET'])
@enable_cors
def lvambience():
    response.headers['Content-type'] = 'application/json'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    return '[1]'


@get('/get_server_status', method=['OPTIONS', 'GET'])
@enable_cors
@autenticar
def get_server_status():
    #ip = sys.argv[1]
    ip = request.query.ip
    #proc = subprocess.Popen('sudo python3 @network_.py ' + ip, shell=True, stdout=subprocess.PIPE,
    #stderr=subprocess.STDOUT)
    proc = subprocess.Popen('echo omega200 | sudo -S python3 @network_.py ' + ip, shell=True, stdout=subprocess.PIPE,
    stderr=subprocess.STDOUT)
    out = proc.communicate()[0]
    try:
        if float(out) > 0:
            return dumps([{"id": 1, "Mensagem": "up"}])
        else:
            return dumps([{"id": 2, "Mensagem": "down"}])
    except:
        return dumps([{"id": 3, "Mensagem": "down"}])