#!/usr/bin/python
from ping3 import ping, verbose_ping
import sys
import os


def pinga(_ip):
    x = ping(ip)
    return x


ip = sys.argv[1]
sys.stdout.write(str(pinga(ip)))

